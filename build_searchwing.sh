#!/bin/bash
set -e

PIGENTAG="2023-05-03-raspios-bullseye"

BUILDDIR="/data/tmp/pi-gen"
#set baseline (to save buildtime) of previous build to continue after this stage2 - only tested with PLAIN builds...
BASELINE="http://breakout.hs-augsburg.de/ciartifacts/companion/cm4-waveshare/20231014_2023-05-03-raspios-bullseye_stage-ros.tar.gz"

#clone pi-gen build repo
git clone --depth 1 --branch ${PIGENTAG} https://github.com/RPI-Distro/pi-gen.git ${BUILDDIR}

#prepare searchwing-pi-gen stages and config
touch ${BUILDDIR}/stage2/SKIP_IMAGES
cp -r stage-ros ${BUILDDIR}
cp -r stage-searchwing ${BUILDDIR}
cp config ${BUILDDIR}

#apply patches
cp patches/* ${BUILDDIR}
cd ${BUILDDIR}
git apply *.patch

#base build on baseline
if [ $BASELINE != "" ]; then
    BUILDWORKDIR=${BUILDDIR}/work/searchwing-pi-waveshare-cm4
    mkdir -p ${BUILDWORKDIR}
    if [[ "$BASELINE" == *"http"* ]]; then # download baseline if its a link
        wget -nc $BASELINE -P /tmp/
        tar -zxf /tmp/$(basename ${BASELINE}) --directory ${BUILDWORKDIR}
    else # link baseline content from local path
        ln -s ${BASELINE}/work/searchwing-pi-waveshare-cm4/stage-ros ${BUILDWORKDIR}
    fi
    touch ${BUILDDIR}/stage0/SKIP
    touch ${BUILDDIR}/stage1/SKIP
    touch ${BUILDDIR}/stage2/SKIP
    touch ${BUILDDIR}/stage-ros/SKIP
fi

if [ ${1} == "docker" ]; then
    #remove last build docker image
    docker rm -v pigen_work
    #build image
    PRESERVE_CONTAINER=1 bash build-docker.sh -c config
elif [ ${1} == "plain" ]; then
    bash build.sh -c config
fi

