#!/bin/bash -e

on_chroot << EOF
export SHELL="/bin/bash"

export ROS_DISTRO="noetic"
export ROS_OS_OVERRIDE=debian:buster
# I very strongly advise to set Python version used by ROS
# otherwise the packages will mix up python2 and python3 during build
# finally leading to the error you encountered (just like me before)
export ROS_PYTHON_VERSION="3"
# disable languages that I don't need
export ROS_LANG_DISABLE="geneus:genlisp:gennodejs"

sh -c 'echo "deb http://packages.ros.org/ros/ubuntu buster main" > /etc/apt/sources.list.d/ros-latest.list'
apt-key adv --keyserver 'hkp://keyserver.ubuntu.com:80' --recv-key C1CF6E31E6BADE8868B172B4F42ED6FBAB17C654
apt-get update
apt-get install -y build-essential tmux cmake git
apt-get install -y python3-rosdep python3-rosinstall-generator python3-vcstool python3-empy python3-catkin-tools python3-osrf-pycommon python3-pip
apt-get install -y ca-certificates
rosdep init
rosdep update # run intentionlly as root even if not recommended
# Remove/add packages to get an installation covering your needs.
rosinstall_generator --rosdistro noetic --deps --tar \
  geometry \
  sensor_msgs \
  image_common \
  vision_opencv \
  image_transport \
  image_transport_plugins \
  image_pipeline \
  image_geometry \
  vision_msgs \
  compressed_image_transport \
  image_proc \
  mavros \
  mavros_extras |
  tee noetic.rosinstall

mkdir -p /opt/ros/src

##import sources using vcs tool (instead of wstool)
vcs import --input noetic.rosinstall /opt/ros/src

# install ros build dependencies
rosdep install --from-paths /opt/ros/src --rosdistro noetic -y --ignore-src --os=debian:buster -v

# for mavros
apt-get install libgeographic-dev -y
/opt/ros/src/mavros/mavros/scripts/install_geographiclib_datasets.sh

# for numpy on pi (see info in https://numpy.org/devdocs/user/troubleshooting-importerror.html#raspberry-pi)
apt-get install libatlas-base-dev -y

# build ros
cd /opt/ros/
catkin config --install
catkin config --skiplist \
  stereo_image_proc \
  depth_image_proc \
  compressed_depth_image_transport \
  theora_image_transport \
  polled_camera \
  image_view \
  image_rotate \
  stereo_msgs
catkin build -j16

#save ros logs to /data instead of /home/user/.ros to avoid home getting full
mkdir -p /data/ros
echo "export ROS_LOG_DIR='/data/ros'" >> /opt/ros/install/setup.bash

echo "source /opt/ros/install/setup.bash" >> ~/.bashrc
echo "source /opt/ros/install/setup.bash" >> /home/${FIRST_USER_NAME}/.bashrc
EOF