#!/bin/bash -e

#compile/install mavlink-router
rm -rf ${ROOTFS_DIR}/home/${FIRST_USER_NAME}/mavlink-router
git clone https://github.com/mavlink-router/mavlink-router ${ROOTFS_DIR}/home/${FIRST_USER_NAME}/mavlink-router

on_chroot << EOF
cd /home/${FIRST_USER_NAME}/mavlink-router
git submodule update --init --recursive
meson setup build .
ninja -C build
sudo ninja -C build install
sudo systemctl enable mavlink-router
EOF

#remove source files
rm -rf ${ROOTFS_DIR}/home/${FIRST_USER_NAME}/mavlink-router

#install mavproxy
on_chroot << EOF
pip install mavproxy
EOF