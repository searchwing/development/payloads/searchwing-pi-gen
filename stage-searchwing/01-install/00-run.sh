#!/bin/bash -e

#boot partition => enable cam/serialport
install -m 644 files/cmdline.txt "${ROOTFS_DIR}/boot/"
install -m 644 files/config.txt "${ROOTFS_DIR}/boot/"

#set static ip for wifi
#echo " " >> ${ROOTFS_DIR}/etc/dhcpcd.conf
#echo "#set static ip for wifi" >> ${ROOTFS_DIR}/etc/dhcpcd.conf
#echo "interface wlan0" >> ${ROOTFS_DIR}/etc/dhcpcd.conf
#echo "static ip_address=${STATIC_WIFI_IP}" >> ${ROOTFS_DIR}/etc/dhcpcd.conf
#echo "static routers=${WIFI_GATEWAY}" >> ${ROOTFS_DIR}/etc/dhcpcd.conf
#echo "static domain_name_servers=${WIFI_DNS}" >> ${ROOTFS_DIR}/etc/dhcpcd.conf

# Disable unused services
# hciuart => Bluetooth service
# bluealsa => Bluetooth Audio ALSA Backend
# bluetooth => Bluetooth service
# rsyslog => Not used
# systemd-timesyncd => timesync not needed
on_chroot << EOF
sudo systemctl disable hciuart
sudo systemctl disable bluealsa
sudo systemctl disable bluetooth
sudo systemctl disable rsyslog
sudo systemctl disable systemd-timesyncd
EOF

#change CM4 dtb for waveshare dual cameras
#from https://www.waveshare.com/wiki/CM4-IO-BASE-B
on_chroot << EOF
cd /tmp
wget https://www.waveshare.net/w/upload/7/75/CM4_dt_blob_Source.zip
unzip -o  CM4_dt_blob_Source.zip -d ./CM4_dt_blob_Source
sudo chmod 777 -R CM4_dt_blob_Source
cd CM4_dt_blob_Source/
sudo  dtc -q -I dts -O dtb -o /boot/dt-blob.bin dt-blob-disp0-double_cam.dts #If you want to use both cameras and DSI0
EOF

# add device tree overlay for sht3x sensor
install -m 644 files/searchwing_sht3x_overlay.dts "${ROOTFS_DIR}/tmp/"
on_chroot << EOF
cd /tmp
sudo dtc -q -@ -I dts -O dtb -o /boot/overlays/searchwing_sht3x.dtbo searchwing_sht3x_overlay.dts
EOF

# Add example files for headless configuration
install -m 644 files/ssh "${ROOTFS_DIR}/boot/"
install -m 644 files/wpa_supplicant.conf "${ROOTFS_DIR}/boot/"

#setup rsync for image to groundstation data transfer
install -m 644 files/rsyncd.conf "${ROOTFS_DIR}/etc/"
on_chroot << EOF
sudo systemctl enable rsync
EOF

# Add custom message of the day for ssh login
install -m 644 files/motd "${ROOTFS_DIR}/etc/"
