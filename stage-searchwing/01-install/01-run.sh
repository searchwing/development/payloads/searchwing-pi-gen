#!/bin/bash -e

#install ros payload
rm -rf ${ROOTFS_DIR}/opt/catkin_ws/src/ros-generic
rm -rf ${ROOTFS_DIR}/opt/catkin_ws/src/ros-vision-generic

FOLDER="${ROOTFS_DIR}/opt/catkin_ws/src/ros-generic"
if [ ! -d "$FOLDER" ] ; then
    git clone -b master https://searchwing-robot:fiVQQM8JF5y4bxVtJyRx@gitlab.com/searchwing/development/payloads/ros-generic $FOLDER
fi
FOLDER="${ROOTFS_DIR}/opt/catkin_ws/src/ros-vision-generic"
if [ ! -d "$FOLDER" ] ; then
    git clone -b master https://searchwing-robot:fiVQQM8JF5y4bxVtJyRx@gitlab.com/searchwing/development/payloads/ros-vision-generic $FOLDER
fi

#source ros for searchwing user
on_chroot << EOF
sudo echo "source /opt/catkin_ws/devel/setup.bash" >> /home/${FIRST_USER_NAME}/.bashrc
EOF

#build searchwing ros payload
on_chroot << EOF
export SHELL="/bin/bash"
/opt/catkin_ws/src/ros-generic/tools/build_searchwing_ros/step3_build_install_searchwing.sh
EOF

on_chroot << EOF
mkdir -p /data/bilder
EOF

#change ownership of files 
on_chroot << EOF
chown -R ${FIRST_USER_NAME}:${FIRST_USER_NAME} /opt/catkin_ws
chown -R ${FIRST_USER_NAME}:${FIRST_USER_NAME} /data
chown -R ${FIRST_USER_NAME}:${FIRST_USER_NAME} /home/${FIRST_USER_NAME}/
EOF

