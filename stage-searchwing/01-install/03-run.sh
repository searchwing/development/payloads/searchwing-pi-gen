#!/bin/bash -e

# Install first boot configuration scripts
install -m 777 files/first_boot_setup.sh "${ROOTFS_DIR}/home/${FIRST_USER_NAME}/" 
on_chroot << EOF
chown -R ${FIRST_USER_NAME}:${FIRST_USER_NAME} /home/${FIRST_USER_NAME}/first_boot_setup.sh
EOF

#add cronjob to set fake-hwclock every minute (instead of every hour) to save current clock for reboot
on_chroot << EOF
sudo echo "*  *    * * *   root    /etc/cron.hourly/fake-hwclock" | sudo tee --append /etc/crontab
EOF

#add passwordless sudo
on_chroot << EOF
sudo echo "${FIRST_USER_NAME} ALL=(ALL:ALL) NOPASSWD:ALL" >> /etc/sudoers
EOF

#added auto reboot in case of kernel panic / system freeze
on_chroot << EOF
sudo echo "kernel.panic = 0" >> /etc/sysctl.conf
EOF

#disable swapping
on_chroot << EOF
sudo dphys-swapfile swapoff
sudo systemctl disable dphys-swapfile
EOF

#limit journal
on_chroot << EOF
sudo echo "SystemMaxUse=30M" >> /etc/systemd/journald.conf
EOF

##disable hdmi
on_chroot << EOF
sudo echo "/opt/vc/bin/tvservice -o" >> /etc/rc.local
sudo echo "/usr/bin/tvservice -o" >> /etc/rc.local
EOF

#install groundstation ssh pub key
install -m 700 -d "${ROOTFS_DIR}/home/${FIRST_USER_NAME}/.ssh"
cat sshkeys/*.pub > "${ROOTFS_DIR}/home/${FIRST_USER_NAME}/.ssh/authorized_keys"
on_chroot << EOF
chown -R ${FIRST_USER_NAME}:${FIRST_USER_NAME} /home/${FIRST_USER_NAME}/.ssh
EOF

#copy bashrc
on_chroot << EOF
cp /home/${FIRST_USER_NAME}/.bashrc /root
EOF

#setup tmux
on_chroot << EOF
sudo echo "set -g mouse on" >> /home/${FIRST_USER_NAME}/.tmux.conf 
sudo echo "set -g mouse on" >> /root/.tmux.conf 
EOF

#ensure correct ownership
sudo chown -R ${FIRST_USER_NAME}:${FIRST_USER_NAME} ${ROOTFS_DIR}/home/${FIRST_USER_NAME}/
sudo chown -R ${FIRST_USER_NAME}:${FIRST_USER_NAME} ${ROOTFS_DIR}/data/
sudo chown -R ${FIRST_USER_NAME}:${FIRST_USER_NAME} ${ROOTFS_DIR}/opt/catkin_ws
