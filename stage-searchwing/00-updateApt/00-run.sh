#!/bin/bash -e

on_chroot << EOF
sudo apt-get update --fix-missing
EOF