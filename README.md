# searchwing-pi-gen

This repo creates a raspbian lite image with all necessary settings and scripts for the searchwing drone.

Created images can be found here:

http://breakout.hs-augsburg.de/ciartifacts/companion/cm4-waveshare/

Features:
* Dynamic IP from Accesspoint
* Predefined User / Password (set to searchwing / cassandra)
* Enabled SSH-Server
* Datatransfer via RSYNC (enabled)
* Automatic resize of filesystem using the `first_boot_setup.sh` script
* Enabled camera connectors
* [Changed device tree for Waveshare Base Board Camera connectors](https://www.waveshare.com/wiki/CM4-IO-BASE-B#CSI_DSI) 
* Enabled serial connection (RX @ Pin10) 
* Mavproxy installed for dataflash log download (just in case...)
* Installs payloads (ROS based image saving - compiles ROS for raspberryOS and searchwing-ROS code)
* Ethernet Gadget @ pi zero USB port: If connected to a laptop, you can login into the pizero via ssh (see section below)

Checkout the `config` file for all predefined values.

## Image creation

### Debian / Ubuntu based

Prerequisites

- Debian Buster or Ubuntu Xenial
- Install dependencies from https://github.com/RPi-Distro/pi-gen#dependencies

Start generation by 

`./build_searchwing.sh plain`

This build method is prefered for CI.

NOTE: If you want to delete the build folder please run 
`sudo ./umount.sh`
first! Otherwise you might damage your machine as there are local ressources mounted into the build folder! See this [issue](https://github.com/RPi-Distro/pi-gen/issues/244) for more info

### Docker based

Prerequisites

- The image build creates around 60GB of output
- Docker installed
- Internet connection
- Current user added to group 'docker', else you must use sudo for `docker run`

Start generation by 

`./build_searchwing.sh docker`

### Build folder

The image will be created in the folder `/tmp/pi-gen/deploy`. 

## How to...

### Add new Python Packages to the image

Edit [./searchwing-pi-gen/stage-searchwing/00-all/01-run.sh](./searchwing-pi-gen/stage-searchwing/00-all/01-run.sh).

### edit image after creation

* unzip image
* get offsets: `sudo parted 2020-02-20-searchwing-pi-lite.img unit s print`
* mount second partition (rootfs) by using its start and size: `sudo mount 2020-02-20-searchwing-pi-lite.img /mnt/point -o loop,offset=$((512*start)),sizelimit=$((512*Size))`
* unmount and rezip

### Login into pi zero via USB gadget

* connect Pi zero USB port via Micro USB to Laptop
* Find link local address: `ping ff02::1%enp7s0f3u2` where `enp7s0f3u2` is the local ethernet device on your laptop. The link local address is usually the second ping response (or the IP which does more slowly answer than the other).
* Login via SSH: `ssh searchwing@fe80::f405:430a:9dc7:db7b%enp7s0f3u2` where `fe80::f405:430a:9dc7:db7b%enp7s0f3u2`is the found link ip.