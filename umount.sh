# killing build.sh process if it is executing
sudo kill $(ps aux | grep '[b]uild.sh' | awk '{print $2}') || :
# umounting partitions in case last build failed
sudo umount --recursive /tmp/pi-gen/work/{stage1,stage0,stage2,stage-ros,stage-searchwing}/rootfs/{dev,proc,sys,tmp,run} || :

echo "leftover mounts"
echo "#############################"
mount | grep pi-gen
